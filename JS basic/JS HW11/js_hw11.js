const form = document.querySelector("form");
const fa_eye = document.querySelectorAll(".fas");
const password = document.querySelector('password');
const fa_eye_slash = document.querySelector(".fa-eye-slash");


function myFunction() {
    form.addEventListener("click", (event) => {
         if (event.target.tagName == 'I') {
            event.target.classList.toggle("fa-eye-slash");
        } 
        if (event.target.classList.contains("fa-eye-slash")) {
            event.target.previousElementSibling.type = "text";
          } else event.target.previousElementSibling.type = "password";        
        });
};
myFunction();


