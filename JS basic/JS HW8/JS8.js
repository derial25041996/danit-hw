let element = document.querySelectorAll('p');
console.log(element);

for (let elem of element) {
  elem.style.background = 'red';
  console.log(elem);
}
// ------------------------------------------------------

let idElement = document.querySelector('#optionsList');
let parent = idElement.parentElement;
let nextElem = idElement.childNodes;

console.log(idElement);
console.log(parent);
console.log(nextElem);
// ----------------------------------------------------------

let paragraph = document.querySelector('#testParagraph');
paragraph.innerHTML = "This is a paragraph";

console.log(paragraph);

// -------------------------------------------------------------

let elem = document.querySelector('.main-header');
let children = elem.children;
for (let child of children) {
  child.classList.add('nav-item')
  console.log(child);
}

console.log(children);
// -------------------------------------------------------------------

let allElem = document.querySelectorAll('.section-title');
console.log(allElem);

