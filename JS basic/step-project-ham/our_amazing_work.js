const section3 = document.querySelector(".section_3");
const section_load = document.querySelector(".section_3__loadImg");
const button = document.querySelector(".button_4");
const but = document.querySelector(".but_4");
const menuItem = document.querySelectorAll(".menuItem");
const menuList = document.querySelector(".menuList");
const images = document.querySelectorAll(".img_amazing");

but.addEventListener("click",(event) => {
  button.classList.toggle("button_4__click");
  section3.classList.toggle("section_3__click");
  section_load.classList.toggle("section_3__loadImg_click");

});

const filterFunc = () =>{

  const filter = (category, items) =>{
    items.forEach((item) =>{
    const isItemFiltered = !item.classList.contains(category)
    const isShowAll = category.toLowerCase() === "all"
    if(isItemFiltered && !isShowAll){
      item.classList.add("hide")
    }else{
      item.classList.remove("hide")
    }
    })
  }

  menuItem.forEach((butt) => {
    butt.addEventListener("click", () => {
      const currentCategory = butt.dataset.name;
      filter(currentCategory,images)
      
    })
  });
};

filterFunc();



const chooseItem = () =>{
  menuList.addEventListener("click",(event) =>{
    document.querySelector(".active_item")
    .classList.remove("active_item");
    event.target.classList.add("active_item");
  })
}

chooseItem();