const itemNav = document.querySelector(".itemnav");
const tabsList = document.querySelectorAll(".div1")

const tabsFunc = () =>{
  itemNav.addEventListener("click",(event)=>{
    document.querySelector(".active").classList.remove("active");
    event.target.classList.add("active");

    document.querySelector(".active_div").classList.remove("active_div");
    tabsList.forEach((item)=>{
      if(item.dataset.title == event.target.dataset.title){
      item.classList.add("active_div");
    }
    })

  })
}

tabsFunc();

