const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

const div = document.createElement('div');
const list = document.createElement('ul');
div.id = "root";
document.body.append(div);
div.appendChild(list);

// const booksList = () => {
//   for (let item of books) {
//     list.innerHTML += "<li>" + item.name + ": " + item.author + "<br>" + "Price: " + item.price + "</li>";
//   }
// }
// booksList();

class Fault extends Error {
  constructor(value) {
    super();
    this.name = "Missing Book prop";
    this.message = `Missing ${value} Elem`;
  }
}

class BookList {
  constructor(book) {
    if (!book.author) {
      throw new Fault("Author");
    } else if (!book.name) {
      throw new Fault("Name");
    } else if (book.price) {
      throw new Fault("Price");
    }
    this.bookAuthor = book.author;
    this.bookName = book.name;
    this.bookPrice = book.price;
  }

  booksList(content) {
    content.insertAdjacentHTML("beforeend",
      `<ul>
        <li>Author: ${this.bookAuthor}</li>
        <li>Name: ${this.bookName}</li>
        <li>Price: ${this.bookPrice}</li>
      </ul>`
    );
  }
}

books.forEach((item) => {
  try {
    new BookList(item).booksList(list);
  } catch (e) {
    if (e instanceof Fault) {
      console.warn(e);
    } else throw e;
  }
});








