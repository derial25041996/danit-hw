// ---------------First Task--------------

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allClients = [...clients1, ...clients2];

function result (allClients){
  return [...new Set(allClients)];
}

console.log(result(allClients));

