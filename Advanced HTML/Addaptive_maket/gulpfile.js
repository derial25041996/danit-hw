import gulp from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import browserSync from 'browser-sync';
import minimizeImg from "gulp-imagemin";
import clean from "gulp-clean";
import concat from "gulp-concat";
import terser from "gulp-terser";
import autoprefixer from "gulp-autoprefixer";
import cssMin from "gulp-clean-css";


const sass = gulpSass(dartSass);
const BS = browserSync.create();


const style = () => gulp.src('./src/styles/**/*.scss')
      .pipe(sass())
      .pipe(gulp.dest('./src/css')); 

const minImg = () => gulp.src("src/images/**/*")
    .pipe(minimizeImg())
    .pipe(gulp.dest("dist/images"));

const cssOper = () => gulp.src("./src/css/*.css")
    .pipe(autoprefixer({cascade:false,}))
    .pipe(concat("main.css"))
    .pipe(cssMin())
    .pipe(gulp.dest("./dist/css"));

const jsOper = () => gulp.src("./src/**/*.js")  
    .pipe(concat("scripts.min.js"))
    .pipe(terser())
    .pipe(gulp.dest("./dist"));
 
export const build = gulp.series(minImg, style, gulp.parallel(cssOper, jsOper));    


export const dev = gulp.series (build, () => {
  BS.init({
      server: {
          baseDir: "./"
      }
  });

  gulp.watch('./src/styles/*', gulp.series( style, (done) => {
      BS.reload();
      done();
  }))
});

  gulp.watch("./src/images/**",gulp.series(minImg,(done) => {
    BS.reload();
    done();
  }));
  
  gulp.watch("./index.html",gulp.series((done) => {
    BS.reload();
    done();
  })) ;

  gulp.watch("./src/styles/*",gulp.series(cssOper,(done) => {
    BS.reload();
    done();
  })) ;

  gulp.watch("./src/**/*",gulp.series(jsOper,(done) => {
    BS.reload();
    done();
  })) ;  

  export const cleanFolder = () => gulp.src("./dist", { read: false, allowEmpty: true }).pipe(clean());


      



